#include <ultra64.h>
#include "functions.h"
#include "variables.h"

#include "prop.h"

extern ActorInfo D_8038EB50;
extern ActorInfo D_8038EB74;
extern ActorInfo D_8038EBA0;
extern ActorInfo D_8038EBD0;
extern ActorInfo D_8038EC14;
extern ActorInfo D_8038EC70;
extern ActorInfo D_8038ECA0;
extern ActorInfo D_8038ECE8;
extern ActorInfo D_8038EE70;
extern ActorInfo D_8038F130;
extern ActorInfo D_8038F160;
extern ActorInfo D_8038F190;
extern ActorInfo D_8038F230;
extern ActorInfo D_8038F6F0;
extern ActorInfo D_8038ED50;
extern ActorInfo D_8038ED98;
extern ActorInfo D_8038EDE0;
extern ActorInfo D_8038EE28;
extern ActorInfo D_8038ED74;
extern ActorInfo D_8038EDBC;
extern ActorInfo D_8038EE04;
extern ActorInfo D_8038EE4C;
extern ActorInfo D_8038F270;
extern ActorInfo D_8038F300;
extern ActorInfo D_8038F35C;
extern ActorInfo D_8038F380;
extern ActorInfo D_8038F3A4;
extern ActorInfo D_8038F3C8;
extern ActorInfo D_8038F3EC;
extern ActorInfo D_8038F410;
extern ActorInfo D_8038F434;
extern ActorInfo D_8038F460;
extern ActorInfo D_8038F4A8;
extern ActorInfo D_8038F4D0;
extern ActorInfo D_8038F614;
extern ActorInfo D_8038F640;
extern ActorInfo D_8038F908;
extern ActorInfo D_8038F8C0;
extern ActorInfo D_8038F720;
extern ActorInfo D_8038F744;
extern ActorInfo D_8038F768;
extern ActorInfo D_8038F78C;
extern ActorInfo D_8038F988;
extern ActorInfo D_8038FA00;
extern ActorInfo D_8038F7D4;
extern ActorInfo D_8038F7B0;
extern ActorInfo D_8038F888;
extern ActorInfo D_8038F7F8;
extern ActorInfo D_8038F81C;
extern ActorInfo D_8038F840;
extern ActorInfo D_8038F864;

#pragma GLOBAL_ASM("asm/nonmatchings/CCW/code_76C0/func_8038DAB0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/CCW/code_76C0/func_8038DB0C.s")

void func_8038DB6C(void)
{
    func_803053E8(&D_8038EB50, func_803272F8, 0X80);
    func_803053E8(&D_8038EB74, func_803272F8, 0X80);
    func_803053E8(&D_8038EBA0, func_803272F8, 0X180);
    func_803053E8(&D_8038EBD0, func_803272F8, 0X200080C);
    func_803053E8(&D_8038EC14, func_803272F8, 0X888);
    func_803053E8(&D_8038EC70, func_803272F8, 0X94C88);
    func_803053E8(&D_8038ECA0, func_803272F8, 0X84C88);
    func_803053E8(&D_8038ECE8, func_803272F8, 0X809A8);
    func_803053E8(&D_8038EE70, func_803272F8, 0XC80);
    func_803053E8(&D_8038F130, func_803272F8, 0X84C2A);
    func_803053E8(&D_8038F160, func_803272F8, 0X881);
    func_803053E8(&D_8038F190, func_803272F8, 0X80D80);
    func_803053E8(&D_8038F230, func_803272F8, 0XD80);
    func_803053E8(&D_8038F6F0, func_803272F8, 0X804);
    func_803053E8(&D_8038ED50, func_803272F8, 0X400);
    func_803053E8(&D_8038ED98, func_803272F8, 0X400);
    func_803053E8(&D_8038EDE0, func_803272F8, 0X400);
    func_803053E8(&D_8038EE28, func_803272F8, 0X400);
    func_803053E8(&D_8038ED74, func_803272F8, 0);
    func_803053E8(&D_8038EDBC, func_803272F8, 0);
    func_803053E8(&D_8038EE04, func_803272F8, 0);
    func_803053E8(&D_8038EE4C, func_803272F8, 0);
    func_803053E8(&D_8038F270, func_803272F8, 0X989);
    func_803053E8(&D_8038F300, func_803272F8, 0X989);
    func_803053E8(&D_8038F35C, func_803272F8, 0X909);
    func_803053E8(&D_8038F380, func_803272F8, 0X80);
    func_803053E8(&D_8038F3A4, func_803272F8, 0X888);
    func_803053E8(&D_8038F3C8, func_803272F8, 0X888);
    func_803053E8(&D_8038F3EC, func_803272F8, 0X80);
    func_803053E8(&D_8038F410, func_803272F8, 0X988);
    func_803053E8(&D_8038F434, func_803272F8, 0X888);
    func_803053E8(&D_8038F460, func_803272F8, 0X880);
    func_803053E8(&D_8038F4A8, func_803272F8, 0X98A);
    func_803053E8(&D_8038F4D0, func_803272F8, 0X988);
    func_803053E8(&D_8038F614, func_803272F8, 0X988);
    func_803053E8(&D_8038F640, func_803272F8, 0X80);
    func_803053E8(&D_8038F908, func_803272F8, 8);
    func_803053E8(&D_8038F8C0, func_803272F8, 8);
    func_803053E8(&D_8038F720, func_803272F8, 0);
    func_803053E8(&D_8038F744, func_803272F8, 0);
    func_803053E8(&D_8038F768, func_803272F8, 0);
    func_803053E8(&D_8038F78C, func_803272F8, 0);
    func_803053E8(&D_8038F988, func_803272F8, 0X2010121);
    func_803053E8(&D_8038FA00, func_803272F8, 0);
    func_803053E8(&D_8038F7D4, func_803272F8, 0X400);
    func_803053E8(&D_8038F7B0, func_803272F8, 0X400);
    func_803053E8(&D_8038F888, func_803272F8, 0X80);
    func_803053E8(&D_8038F7F8, func_803272F8, 0X400);
    func_803053E8(&D_8038F81C, func_803272F8, 0X400);
    func_803053E8(&D_8038F840, func_803272F8, 0X400);
    func_803053E8(&D_8038F864, func_803272F8, 0X400);
}