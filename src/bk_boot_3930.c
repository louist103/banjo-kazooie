#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3930/func_80002D30.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3930/func_80002D5C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3930/func_80002D98.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3930/func_80002DD4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3930/func_80002E00.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3930/func_80002E3C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3930/func_80002E98.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3930/func_80002EC8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3930/func_80002F28.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3930/func_80002FC4.s")
