#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_CA0/func_80387090.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_CA0/func_803870BC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_CA0/func_8038711C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_CA0/func_8038718C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_CA0/func_80387308.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_CA0/func_80387488.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_CA0/func_8038756C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_CA0/func_80387850.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_CA0/func_80387890.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_CA0/func_803878B0.s")
