#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387960.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_803879F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387A54.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387AC0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387B24.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387B8C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387BEC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387C5C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387D80.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387E20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387F18.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387F44.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80387F88.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_80388154.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_803881E8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_803882B4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/RBB/code_1570/func_803882F4.s")
