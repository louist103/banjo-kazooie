#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_2F5F0/func_8026D010.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_2F5F0/func_8026D1B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_2F5F0/func_8026D2AC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_2F5F0/func_8026D380.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_2F5F0/func_8026D430.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_2F5F0/func_8026D500.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_2F5F0/func_8026D704.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_2F5F0/func_8026D880.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_2F5F0/func_8026DA9C.s")
