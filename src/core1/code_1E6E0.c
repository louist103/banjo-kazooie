#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1E6E0/SnSPayload_calcChecksum.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1E6E0/func_8025C240.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1E6E0/func_8025C288.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1E6E0/func_8025C29C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1E6E0/func_8025C2E0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1E6E0/func_8025C320.s")
