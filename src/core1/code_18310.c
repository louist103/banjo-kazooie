#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80255D30.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80255D44.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80255D58.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80255D70.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80255E58.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80255F14.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80255F74.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80255FE4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256034.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256064.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802560D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256280.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802562DC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256378.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802563B8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256450.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802564F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256558.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802565E0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256664.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256740.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025686C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256900.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256990.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256A24.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256AB4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256B54.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256C60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256D0C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256E24.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256F44.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80256FE0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025715C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257204.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257248.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025727C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257424.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257594.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802575BC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257618.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257658.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257680.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802576F8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025773C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025778C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802578A4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257918.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802579B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257A44.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257A6C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257AD4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/mlMap_f.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257BFC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257C48.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257C60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257CC0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257CF8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257D30.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257DB0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257E14.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257EA8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257ED8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80257F18.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025801C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258108.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258210.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802582EC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258368.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802583D8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258424.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802584FC.s")
// s32 func_802584FC(f32 *arg0, f32 *min, f32 *max)
// {
//     return (min[0] < arg0[0] && arg0[0] < max[0]
//     && min[1] < arg0[1] && arg0[1] < max[1]
//     && min[2] < arg0[2] && arg0[2] < max[2]);
// }

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802585E0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258640.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802586B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258708.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258780.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802587BC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802587EC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/mlNormalizeAngle.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802588B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/max_f.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258904.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025892C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258948.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258964.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258994.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802589CC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802589E4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258A4C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/clear_vec3f.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/copy_vec3f.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258BC0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258BF4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258C28.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258C48.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258C7C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/mlScale.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258CDC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258D68.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258DA8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258DE8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258E24.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258E60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258EF4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80258F88.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025901C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80259198.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802591D8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80259254.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802592C4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80259328.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80259384.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80259400.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80259430.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025947C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80259554.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_802596AC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025975C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_80259808.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_18310/func_8025982C.s")
