#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_27AB0/osSetIntMask.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_27AB0/func_80265570.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_27AB0/func_80265580.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_27AB0/func_8026569C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_27AB0/func_8026582C.s")
