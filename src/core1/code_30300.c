#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_30300/func_8026DD20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_30300/func_8026DD7C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_30300/func_8026DDE4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_30300/func_8026E1FC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_30300/func_8026E394.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_30300/func_8026E5F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_30300/func_8026E6EC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_30300/func_8026EA0C.s")
