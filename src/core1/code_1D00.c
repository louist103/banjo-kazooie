#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023F720.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023F770.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023F88C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023FA4C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023FA64.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023FB1C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023FBB8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023FE80.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023FFAC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023FFD4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_8023FFE4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_802401C4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_80240204.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_802403B8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_802403F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_80240538.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_80240570.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_802405AC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_802405B8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_802405C4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_802405D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_1D00/func_802405DC.s")
