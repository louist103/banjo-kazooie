#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_80267320.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_802673C0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_80267460.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_80267524.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_8026757C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_80267584.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_8026787C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_80267990.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_80267AFC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_80267B04.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/func_80267C00.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_29900/__alCSeqNextDelta.s")
