#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265A90.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265ABC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265AF8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265B34.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265B60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265B9C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265BF8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265C28.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265C88.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265D24.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265D50.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265DD0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_28070/func_80265DF0.s")
