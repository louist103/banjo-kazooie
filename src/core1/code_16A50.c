#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254470.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254490.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_8025449C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_8025456C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254608.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254630.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254658.s")

s32 func_802546D0(void){ return 0x210520; }

s32 func_802546DC(void){ return 0; }

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_802546E4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_802546FC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254710.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_8025484C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254898.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254908.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254960.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_8025496C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_8025498C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_802549BC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254A60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254B84.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254BC4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254BD0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254C98.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/malloc.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254F90.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80254FD0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/free.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255170.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255198.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255200.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255300.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255340.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_8025534C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/realloc.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255498.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_802554C0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255524.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_802555C4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_802555D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_802555DC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255724.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255774.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255888.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_802558D8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255920.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255978.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255980.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_802559A0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255A04.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255A14.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255A20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255A30.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255A3C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255ACC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255AE4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core1/code_16A50/func_80255B08.s")
