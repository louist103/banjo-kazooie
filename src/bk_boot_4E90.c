#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_4E90/func_80004290.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_4E90/func_800042C0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_4E90/func_80004310.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_4E90/func_80004354.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_4E90/func_80004380.s")
