#include <ultra64.h>
#include "functions.h"
#include "variables.h"

extern ActorInfo D_80389AA0;
extern ActorInfo D_80389E44;
extern ActorInfo D_80389E68;
extern ActorInfo D_80389E8C;
extern ActorInfo D_80389AD0;
extern ActorInfo D_80389B00;
extern ActorInfo D_80389B24;
extern ActorInfo D_80389B90;
extern ActorInfo D_80389BB4;
extern ActorInfo D_80389C90;
extern ActorInfo D_80389CB4;
extern ActorInfo D_80389CD8;
extern ActorInfo D_80389CFC;
extern ActorInfo D_80389D20;
extern ActorInfo D_80389D44;
extern ActorInfo D_80389D68;
extern ActorInfo D_80389D8C;
extern ActorInfo D_80389DB0;
extern ActorInfo D_80389DD4;
extern ActorInfo D_80389DF8;
extern ActorInfo D_80389E1C;

void func_80387DA0(void)
{
    func_803053E8(&D_80389AA0, func_803272F8, 0X4080);
    func_803053E8(&D_80389E44, func_803272F8, 0X10080);
    func_803053E8(&D_80389E68, func_803272F8, 0X10080);
    func_803053E8(&D_80389E8C, func_803272F8, 0X10080);
    func_803053E8(&D_80389AD0, func_803272F8, 0X80);
    func_803053E8(&D_80389B00, func_803272F8, 0X80);
    func_803053E8(&D_80389B24, func_803272F8, 0X80);
    func_803053E8(&D_80389B90, func_803272F8, 0X80);
    func_803053E8(&D_80389BB4, func_803272F8, 0X80);
    func_803053E8(&D_80389C90, func_803272F8, 0X2488);
    func_803053E8(&D_80389CB4, func_803272F8, 0X2488);
    func_803053E8(&D_80389CD8, func_803272F8, 0X2488);
    func_803053E8(&D_80389CFC, func_803272F8, 0X2488);
    func_803053E8(&D_80389D20, func_803272F8, 0X2488);
    func_803053E8(&D_80389D44, func_803272F8, 0X2488);
    func_803053E8(&D_80389D68, func_803272F8, 0X2488);
    func_803053E8(&D_80389D8C, func_803272F8, 0X2488);
    func_803053E8(&D_80389DB0, func_803272F8, 0X2488);
    func_803053E8(&D_80389DD4, func_803272F8, 0X2488);
    func_803053E8(&D_80389DF8, func_803272F8, 0X2488);
    func_803053E8(&D_80389E1C, func_803272F8, 0X2488);
}