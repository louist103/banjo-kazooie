#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/CC/code_BF0/func_80386FE0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/CC/code_BF0/func_803870E0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/CC/code_BF0/func_803870EC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/CC/code_BF0/func_803870F8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/CC/code_BF0/func_80387510.s")

#pragma GLOBAL_ASM("asm/nonmatchings/CC/code_BF0/func_80387584.s")
