#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_80335220.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_8033531C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_80335354.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_80335394.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_803353BC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_803353F4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_80335418.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_8033543C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_80335470.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_80335494.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_803354B4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_803354C8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_803354EC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_AE290/func_80335520.s")
