#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80298D70.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80298F14.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_802990B4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80299118.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_802991A8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/player_setMovingYaw.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_802991D8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_802991FC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80299210.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/player_getYaw.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/player_getMovingYaw.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80299234.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80299248.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80299254.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_802992F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_8029932C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80299338.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_802993C8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_8029957C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80299588.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80299594.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11DE0/func_80299628.s")
