#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/drOneToGo/func_802AEB60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/drOneToGo/func_802AEC08.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/drOneToGo/func_802AEC28.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/drOneToGo/func_802AEC70.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/drOneToGo/func_802AEC78.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/drOneToGo/func_802AECE4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/drOneToGo/func_802AEDC8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/drOneToGo/func_802AEDE8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/drOneToGo/func_802AEE48.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/drOneToGo/func_802AEE9C.s")
