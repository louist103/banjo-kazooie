#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B0A60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B0A6C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B0A78.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B0BA8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B0BE4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B0CD8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B0D1C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B0EBC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B0EF0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B0F88.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jig/func_802B1090.s")
