#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/stand/func_802B4870.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/stand/func_802B488C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/stand/func_802B4998.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/stand/func_802B4A10.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/stand/func_802B4D20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/stand/func_802B5248.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/stand/func_802B5278.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/stand/func_802B52B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/stand/func_802B5350.s")
