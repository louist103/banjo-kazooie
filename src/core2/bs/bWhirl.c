#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA400.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA460.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA4EC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA510.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA58C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA630.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA6DC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA728.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA754.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA798.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA7FC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA81C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA87C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA95C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AA97C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AAAC4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AAC1C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AAC44.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AACF0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AAD2C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AAD4C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AAD6C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AAD94.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AADBC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AAE08.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bWhirl/func_802AAE4C.s")
