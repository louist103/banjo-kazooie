#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/carry/func_802AAE80.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/carry/func_802AAEE0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/carry/func_802AAF24.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/carry/func_802AAFD0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/carry/func_802AB018.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/carry/func_802AB038.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/carry/func_802AB0DC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/carry/func_802AB164.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/carry/func_802AB184.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/carry/func_802AB1A4.s")
