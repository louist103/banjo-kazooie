#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B6D00.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B6E44.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B6EB0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B6EBC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B6EF4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B6F20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B6F9C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B6FA8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B6FC8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B70C4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7260.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B735C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B74F8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7614.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B77B8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B796C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7B88.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7BB8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7C30.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7D4C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7DC0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7DE0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7E00.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7E6C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7ECC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B7F28.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8048.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B80D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8110.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B813C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8190.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B81F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8284.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8330.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B83BC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B83E4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8454.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B84F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8518.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8654.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8888.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B88B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B894C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8AB4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8AD4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8BF8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8C4C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8C84.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8CA4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8CC4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8CE4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8D04.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8D24.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8D44.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B8F04.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9010.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9060.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9088.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B90A8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B90D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9130.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B917C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B91A4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9214.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B92DC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9304.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9440.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9578.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B95A0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B963C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B976C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B978C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9830.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9880.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B98C0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B990C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9934.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/twirl/func_802B9954.s")
