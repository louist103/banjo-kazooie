#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1100.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B112C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1354.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1614.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1660.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1750.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1920.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1928.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1A54.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1BCC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1BF4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1CF8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1DA4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1DC4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1DD0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1E80.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/jump/func_802B1F6C.s")
