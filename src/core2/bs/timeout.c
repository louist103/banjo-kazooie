#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/timeout/func_802B6270.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/timeout/func_802B6314.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/timeout/func_802B63C8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/timeout/func_802B63F8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/timeout/func_802B64D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/timeout/func_802B6500.s")
