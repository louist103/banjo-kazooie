#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A51D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5208.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A524C.s")

int bslongleg_inSet(s32 move_indx){
    return (move_indx == bs_longleg_idle)
    || (move_indx == bs_longleg_walk)
    || (move_indx == bs_longleg_jump)
    || (move_indx == bs_longleg_exit)
    || (move_indx == bs_longleg_slip)
    || (move_indx == 0x9b)
    || (move_indx == 0x62);
}

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A531C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5374.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5404.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A54A8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5548.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A55C0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5684.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5718.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5744.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5798.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A587C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A589C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A592C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5A90.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5AB0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5B34.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5BB4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5C34.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5CF4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5D20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A5E70.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A611C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A6144.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A624C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A6368.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A6388.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A6394.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A63F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A6450.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A6478.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A64A0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bLongLeg/func_802A64E0.s")
