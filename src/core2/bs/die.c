#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/die/func_802ADE00.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/die/func_802ADE20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/die/func_802ADEA0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/die/func_802AE058.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/die/func_802AE30C.s")
