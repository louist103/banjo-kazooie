#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ABDC0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ABE04.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ABE70.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ABF54.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ABFBC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC024.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC0D4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC178.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC224.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC24C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC2BC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC380.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC3A8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC4DC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC6D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC6F8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC78C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC8CC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AC8EC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACA50.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACAF8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACB38.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACB58.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACB78.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACB98.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACBB8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACBD8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACBF8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACDA0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACEFC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACF4C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACF58.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802ACFCC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD024.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD168.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD18C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD1D4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD288.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD2A8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD318.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD328.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD3A0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD3AC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD530.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD56C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD5C0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD614.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD654.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD674.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/croc/func_802AD69C.s")
