#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/buster/func_8029FB20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/buster/func_8029FB30.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/buster/func_8029FC4C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/buster/func_8029FC58.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/buster/func_8029FC64.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/buster/func_8029FD8C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/buster/func_802A0278.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/buster/func_802A02B4.s")
