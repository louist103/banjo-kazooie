#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E3E0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E448.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E48C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E4EC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E554.s")

int bsant_inSet(s32 move_indx){
    return (move_indx == bs_ant_idle)
    || (move_indx == bs_ant_walk)
    || (move_indx == bs_ant_jump)
    || (move_indx == bs_ant_fall)
    || (move_indx == 0x3e)
    || (move_indx == 0x43)
    || (move_indx == 0x8e)
    || (move_indx == 0x92);
}

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E618.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E6B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E73C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E764.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E7D4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E8A0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029E8C8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029EA04.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029EB94.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029EBBC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029EC5C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029ED3C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029ED5C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029EEC8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029EF68.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029EFA8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029EFC8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029EFE8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F008.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F028.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F048.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F068.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F218.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F348.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F398.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F3F4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F440.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F468.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F490.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/ant/func_8029F4B0.s")
