#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3350.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A339C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A33D8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3404.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3430.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A34C8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A354C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3648.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A36D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3778.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A37F8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3820.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A38DC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3994.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A39BC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3AA8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3F70.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3F90.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A3F9C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4078.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A40E0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A411C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4404.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4430.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4548.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4664.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A46C8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4748.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A47E0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A48B4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4A40.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4A78.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4C34.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4C88.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4CD0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4CF0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4D10.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4D30.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4D50.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4D70.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4D90.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4EC8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4F44.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4F74.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A4FC8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A503C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A505C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A50B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A50D8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A50F8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A5120.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A5190.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/bs/bFly/func_802A51C0.s")
