#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298800.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298850.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_8029887C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298890.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_802988DC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_8029892C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298970.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298A64.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298A84.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298AD0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298C70.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298CB4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298CE0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298D04.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298D28.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/player_getRoll.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298D48.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_11870/func_80298D54.s")
