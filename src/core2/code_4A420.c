#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D13B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D1410.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D1468.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D1680.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D16AC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D1724.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D1748.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D17A0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D17A8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D181C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D186C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D18B4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D1970.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D1998.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D1B8C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D1CF0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D2964.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D2B94.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D2C24.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D2CB8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4A420/func_802D2CDC.s")
