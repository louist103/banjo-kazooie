#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349BB0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349C3C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349C8C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349CB0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349CD8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349D00.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349D60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349EC0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349EE4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349F50.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_80349FB0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_8034A054.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_8034A06C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_C2C20/func_8034A0EC.s")
