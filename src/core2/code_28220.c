#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28220/func_802AF1B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28220/func_802AF268.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28220/func_802AF3B4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28220/func_802AF4B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28220/func_802AF4E0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28220/func_802AF550.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28220/func_802AF604.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28220/func_802AF668.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28220/func_802AF768.s")
