#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DAC0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DAD8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DAE4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DB04.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DB14.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DD60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DE0C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DE6C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DEEC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DF20.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DF48.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_6B30/func_8028DFB8.s")
