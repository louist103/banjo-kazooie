#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802AF7A0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802AF88C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802AF900.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802AFADC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802AFB0C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802AFB94.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802AFBA0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802AFBAC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802AFBB8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802AFFAC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B0060.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B014C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B016C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B01B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B01BC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B01C8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B01F8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B0464.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B051C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B0560.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B0A04.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_28810/func_802B0A54.s")
