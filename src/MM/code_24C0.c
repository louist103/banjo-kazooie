#include <ultra64.h>
#include "functions.h"
#include "variables.h"

#include "prop.h"

extern ActorInfo chhutInfo;
extern ActorInfo chchimpystump;
extern ActorInfo chgrublinInfo;
extern ActorInfo chcongaInfo;
extern ActorInfo chorangeInfo;
extern ActorInfo chjujuhitboxInfo;
extern ActorInfo chjujuInfo;
extern ActorInfo chorangepadInfo;
extern ActorInfo chlmonkeyInfo;

void func_803888B0(void){
    func_803053E8( &chhutInfo,          func_803272F8,     0X400);
    func_803053E8( &chchimpystump,      func_803272F8,       0x0);
    func_803053E8( &chgrublinInfo,      func_803272F8, 0X2000121);
    func_803053E8( &chcongaInfo,        func_803272F8, 0X2000160);
    func_803053E8( &chorangeInfo,       func_803272F8,       0x0);
    func_803053E8( &chjujuhitboxInfo,   func_803272F8,       0x0);
    func_803053E8( &chjujuInfo,         func_803272F8,    0X4004);
    func_803053E8( &chorangepadInfo,    func_803272F8,      0X40);
    func_803053E8( &chlmonkeyInfo,      func_803272F8,     0X100);
}