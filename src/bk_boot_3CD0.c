#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3CD0/func_800030D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3CD0/func_80003100.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3CD0/func_80003140.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3CD0/func_800037E4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/bk_boot_3CD0/func_800038CC.s")
