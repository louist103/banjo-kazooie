#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80387910.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_803879B8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80387A80.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80387B48.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80387C28.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80387DCC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80387DF4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80387E64.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80387F00.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80387FA8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80388080.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80388CF0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80388D48.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80388D80.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80388E48.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80388EB0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80388F24.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80388FA0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80389214.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_803892C8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80389494.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80389610.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80389948.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_80389984.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_803899B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_1520/func_8038A3B0.s")
