#include <ultra64.h>
#include "functions.h"
#include "variables.h"


#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_D60/func_80387150.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_D60/func_803871FC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_D60/func_80387258.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_D60/func_80387288.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_D60/func_80387314.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_D60/func_803874C4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_D60/func_80387514.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_D60/func_80387690.s")

#pragma GLOBAL_ASM("asm/nonmatchings/SM/code_D60/func_80387764.s")
